//
//  ContentView.swift
//  My Heroes
//
//  Created by Munabeno on 20/01/2020.
//  Copyright © 2020 Munabeno. All rights reserved.
//
import SwiftUI

struct ContentView: View {
    
    var body: some View {
        NavigationView {
            VStack {
                NavigationLink(destination: CharacterPageSelected()) {
                    Text("Select Character")
                }
            }
        }
    }
}

#if DEBUG
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
