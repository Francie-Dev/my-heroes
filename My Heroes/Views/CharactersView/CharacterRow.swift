//
//  CharacterRow.swift
//  My Heroes
//
//  Created by Munabeno on 27/01/2020.
//  Copyright © 2020 Munabeno. All rights reserved.
//
import SwiftUI

struct CharacterRow: View {
    
    var categoryName:String
    var characters:[Character]
    
    var body: some View {
        VStack(alignment: .leading) {
            
            Text(self.categoryName)
                .font(.title)
            
            ScrollView(.horizontal){
                HStack(alignment: .top) {
                    ForEach(characters, id: \.id){ character in
                        NavigationLink(destination: CharacterDetail(character: character)){
                                CharacterItem(data: character)
                        }
                    }
                }
            }
        }
    }
}


#if DEBUG
struct CharacterRow_Previews : PreviewProvider {
    static var previews: some View {
        CharacterRow(categoryName: "Gentil", characters: characterData)
    }
}
#endif
