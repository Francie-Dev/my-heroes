//
//  CharacterDetail.swift
//  My Heroes
//
//  Created by Munabeno on 28/01/2020.
//  Copyright © 2020 Munabeno. All rights reserved.
//

import SwiftUI

struct CharacterDetail: View {
    
    var character:Character
    
    var body: some View {
        VStack(alignment: .center) {
            Image(character.imageName)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 300, height: 350)
            
            Text(character.name)
                .font(.title)
                .padding(.bottom, 10)
            
            Text("Vie \(character.endurance)")
                .padding(.bottom, 5)
            
            Text("Force \(character.force)")
                .padding(.bottom, 5)
            
            Text("Defense Physique \(character.defensePhysique)")
                .padding(.bottom, 5)
            
            Text("Spirituel \(character.spirituel)")
                .padding(.bottom, 5)
            
            Text("Defense Magique \(character.defenseMagique)")
                .padding(.bottom, 5)
        }
        .padding(.top, 0.0)
    }
}

#if DEBUG
struct CharacterDetail_Previews : PreviewProvider {
    static var previews: some View {
        CharacterDetail(character: characterData[0])
    }
}
#endif
