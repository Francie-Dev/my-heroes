//
//  CharacterSlected.swift
//  My Heroes
//
//  Created by Munabeno on 28/01/2020.
//  Copyright © 2020 Munabeno. All rights reserved.
//

import SwiftUI

struct CharacterPageSelected: View {
    
    var categories:[String:[Character]] {
        .init(
            grouping: characterData,
            by: {$0.category.rawValue}
        )
    }
    
    var body: some View{
        List(categories.keys.sorted(), id: \String.self){ key in
            CharacterRow(categoryName: "\(key)".uppercased(), characters: self.categories[key]!)
                .frame(height: 350)
        }
    }
}

#if DEBUG
struct CharacterPageSelected_Previews : PreviewProvider {
    static var previews: some View {
        CharacterPageSelected()
    }
}
#endif
