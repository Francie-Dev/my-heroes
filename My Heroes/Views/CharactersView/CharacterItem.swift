//
//  CharacterItem.swift
//  My Heroes
//
//  Created by Munabeno on 20/01/2020.
//  Copyright © 2020 Munabeno. All rights reserved.
//

import SwiftUI

struct CharacterItem: View {
    
    let data:Character
    
    var body: some View {
        VStack(spacing: 16.0) {
            Image(data.imageName)
                .resizable()
                .renderingMode(.original)
                .scaledToFill()
                .frame(height: 200)
                .shadow(radius: 2)
                .padding()

            VStack(spacing: 5.0) {
                Text(data.name)
                    .font(.headline)
                    .foregroundColor(Color.primary)
            }
        }
        .padding()
        .overlay(
            RoundedRectangle(cornerRadius: 16)
                .stroke(Color.gray, lineWidth: 1)
        )
    }
}

#if DEBUG
struct CharacterItem_Previews : PreviewProvider {
    static var previews: some View {
        CharacterItem(data: characterData[1])
    }
}
#endif
