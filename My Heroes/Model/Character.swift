//
//  character.swift
//  My Heroes
//
//  Created by Munabeno on 20/01/2020.
//  Copyright © 2020 Munabeno. All rights reserved.
//

import SwiftUI

struct Character: Hashable, Codable, Identifiable {
    var id:Int
    var name:String
    var imageName:String
    var category:Category
    var endurance:Int
    var force:Int
    var spirituel:Int
    var defensePhysique:Int
    var defenseMagique:Int
    var vitesse:Int
    
    enum Category: String, CaseIterable, Codable, Hashable {
        case gentil = "Eleves"
        case vilain = "Vilains"
    }
    
}
